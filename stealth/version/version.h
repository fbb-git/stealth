#ifndef INCLUDED_VERSION_
#define INCLUDED_VERSION_

#include "../VERSION"

namespace Icmbuild
{
    char const author[]  = AUTHOR;
    char const version[] = VERSION;
    char const years[]   = YEARS;
};

        
#endif
